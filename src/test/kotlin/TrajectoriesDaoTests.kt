import gr.antzam.gis.Application
import gr.antzam.gis.dao.TrajectoriesDao
import gr.antzam.gis.exceptions.MmsiNotFoundException
import gr.antzam.gis.exceptions.TrajectoryNotFoundException
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(Application::class))
class TrajectoriesDaoTests {

    @Autowired lateinit var trajectoriesDao: TrajectoriesDao

    @Test
    fun shouldReturnAllShipsMmsi() {
        assertThat(trajectoriesDao.getAllShipsMmsi().size).isEqualTo(1160)
    }

    @Test
    fun shouldReturnTrajectoryBoundariesForMmsi() {
        val trajectoryBoundaries = trajectoriesDao.getTrajectoryBoundariesForMmsi(229744000)
        assertThat(trajectoryBoundaries.size).isEqualTo(7)
        for (boundary in trajectoryBoundaries) {
            assertThat(boundary.start).isBeforeOrEqualsTo(boundary.end)
        }
    }

    @Test
    fun shouldReturnValidGeoJsonForMmsiTrajectory() {
        val trajectoryGeoJson = trajectoriesDao.getTrajectoryAsGeoJson(229744000, 1)
        assertThat(trajectoryGeoJson).startsWith("{\"type\":\"LineString\"")
    }

    @Test
    fun shouldReturnPositionsWithSpeedsForMmsiTrajectory() {
        val trajectoryPoints = trajectoriesDao.getPositionsWithSpeedsForTrajectory(229744000, 1)
        assertThat(trajectoryPoints.size).isEqualTo(144)
    }

    @Test(expected = MmsiNotFoundException::class)
    fun shouldThrowExceptionForNonexistantMmsi() {
        trajectoriesDao.getTrajectoryBoundariesForMmsi(2)
    }

    @Test(expected = TrajectoryNotFoundException::class)
    fun shouldThrowExceptionForNonexistantTrajectory() {
        trajectoriesDao.getTrajectoryAsGeoJson(229744000, 100)
    }

    @Test(expected = TrajectoryNotFoundException::class)
    fun getPositionWithSpeedShouldTrowExceptionForNonexistantTrajectory() {
        trajectoriesDao.getPositionsWithSpeedsForTrajectory(229744000, 100)
    }

}
