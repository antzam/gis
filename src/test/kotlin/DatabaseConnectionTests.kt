import gr.antzam.gis.Application
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.jdbc.JdbcTestUtils.countRowsInTable
import javax.sql.DataSource

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(Application::class))
class DatabaseConnectionTests {

    @Autowired lateinit var dataSource: DataSource
    var jdbcTemplate = JdbcTemplate()

    @Before
    fun setUp() {
        jdbcTemplate = JdbcTemplate(dataSource)
    }

    @Test
    fun shouldContainPositionsTable() {
        assertThat(countRowsInTable(jdbcTemplate, "positions")).isGreaterThan(0)
    }

    @Test
    fun shouldContainStopsTable() {
        assertThat(countRowsInTable(jdbcTemplate, "stops")).isGreaterThan(0)
    }

    @Test
    fun shouldContainTrajectoriesTable() {
        assertThat(countRowsInTable(jdbcTemplate, "trajectories")).isGreaterThan(0)
    }

    @Test
    fun shouldContain3dTrajectoriesView() {
        assertThat(countRowsInTable(jdbcTemplate, "trajectories_3d")).isGreaterThan(0)
    }

}
