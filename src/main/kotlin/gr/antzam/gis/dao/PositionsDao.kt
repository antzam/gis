package gr.antzam.gis.dao

import gr.antzam.gis.models.Position
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import java.time.Instant
import javax.sql.DataSource

@Repository
open class PositionsDao @Autowired constructor(dataSource: DataSource) {

    val jdbcTemplate = JdbcTemplate(dataSource)

    open fun getPositionsAtInstant(instant: Instant): List<Position> {
        val queryString = """
            SELECT mmsi, lon, lat
            FROM (SELECT
                    t, mmsi,
                    ST_X(ST_Transform(position, 4326)) AS lon,
                    ST_Y(ST_Transform(position, 4326)) AS lat,
                    rank() OVER (PARTITION BY mmsi ORDER BY abs(extract(EPOCH FROM t - to_timestamp(?)))) AS rank
                   FROM positions
                   WHERE t BETWEEN to_timestamp(?) - '59 secs' :: INTERVAL AND to_timestamp(?) + '59 secs' :: INTERVAL) AS p
            WHERE rank = 1
        """
        val result = jdbcTemplate.query(queryString,
                RowMapper<Position> { rs, _ ->
                    Position(
                            mmsi = rs?.getInt("mmsi") ?: 0,
                            lon = rs?.getFloat("lon") ?: 0.0f,
                            lat = rs?.getFloat("lat") ?: 0.0f
                    )
                },
                instant.epochSecond,
                instant.epochSecond,
                instant.epochSecond)
        return result
    }

}
