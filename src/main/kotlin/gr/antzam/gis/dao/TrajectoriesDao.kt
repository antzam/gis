package gr.antzam.gis.dao

import gr.antzam.gis.exceptions.MmsiNotFoundException
import gr.antzam.gis.exceptions.TrajectoryNotFoundException
import gr.antzam.gis.models.PositionWithSpeed
import gr.antzam.gis.models.TimeBoundary
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import java.util.*
import javax.sql.DataSource

@Repository
open class TrajectoriesDao @Autowired constructor(dataSource: DataSource) {

    private val jdbcTemplate = JdbcTemplate(dataSource)

    open fun getAllShipsMmsi(): List<Int> {
        val queryString = "SELECT min(mmsi) AS mmsi FROM trajectories GROUP BY mmsi ORDER BY mmsi"
        val result = jdbcTemplate.queryForList(queryString, Int::class.java)
        return result
    }

    open fun getTrajectoryBoundariesForMmsi(mmsi: Int): List<TimeBoundary> {
        val queryString = """
            SELECT trajectory, min(t) AS start, max(t) AS "end"
            FROM trajectories
            WHERE mmsi = ?
            GROUP BY trajectory
            ORDER BY trajectory
        """
        val result = jdbcTemplate.query(queryString,
                RowMapper<TimeBoundary> { rs, rowNum ->
                    TimeBoundary(
                            id = rs?.getInt("trajectory") ?: rowNum,
                            start = Date(rs?.getTimestamp("start")?.time ?: 0),
                            end = Date(rs?.getTimestamp("end")?.time ?: 0)
                    )
                },
                mmsi)
        if (result.size == 0) {
            throw MmsiNotFoundException("No trajectories found for vessel with MMSI $mmsi")
        }
        return result
    }

    open fun getTrajectoryAsGeoJson(mmsi: Int, trajectory: Int): String {
        val queryString = """
            SELECT ST_AsGeoJSON(ST_MakeLine(ST_Transform(position, 4326) ORDER BY p.t))
            FROM positions AS p JOIN trajectories AS t
                ON p.mmsi = t.mmsi AND p.t = t.t
            WHERE p.mmsi = ? AND trajectory = ?
            GROUP BY p.mmsi, trajectory
        """
        try {
            val result = jdbcTemplate.queryForObject(queryString, String::class.java, mmsi, trajectory)
            return result
        } catch (e: EmptyResultDataAccessException) {
            throw TrajectoryNotFoundException("Trajectory $trajectory not found for vessel with MMSI $mmsi")
        }
    }

    open fun getPositionsWithSpeedsForTrajectory(mmsi: Int, trajectory: Int): List<PositionWithSpeed> {
        val queryString = """
            SELECT
              t.t,
              ST_X(ST_Transform(position, 4326)) AS lon,
              ST_Y(ST_Transform(position, 4326)) AS lat,
              CASE
                WHEN lag(t.t, 1) OVER w IS NULL THEN
                  1.943844 * st_distance(lead(position, 1) OVER w, position) / extract(EPOCH FROM lead(t.t, 1)OVER w - t.t)
                WHEN lead(t.t, 1) OVER w IS NULL THEN
                  1.943844 * st_distance(position, lag(position, 1) OVER w) / extract(EPOCH FROM t.t - lag(t.t, 1) OVER w)
                ELSE
                  1.943844 * st_distance(lead(position, 1) OVER w, lag(position, 1) OVER w) /
                    extract(EPOCH FROM lead(t.t, 1) OVER w - lag(t.t, 1) OVER w)
              END AS speed
            FROM positions AS p JOIN trajectories AS t
              ON p.mmsi = t.mmsi AND p.t = t.t
            WHERE p.mmsi = ? AND trajectory = ?
            WINDOW w AS (PARTITION BY t.mmsi, t.trajectory ORDER BY t.t)
            ORDER BY p.t
        """
        val result = jdbcTemplate.query(queryString,
                RowMapper<PositionWithSpeed> { rs, _ ->
                    PositionWithSpeed(
                            time = Date(rs?.getTimestamp("t")?.time ?: 0),
                            lon = rs?.getFloat("lon") ?: 0.0f,
                            lat = rs?.getFloat("lat") ?: 0.0f,
                            speed = rs?.getFloat("speed") ?: 0.0f
                    )
                },
                mmsi, trajectory)
        if (result.size == 0) {
            throw TrajectoryNotFoundException("Trajectory $trajectory not found for vessel with MMSI $mmsi")
        }
        return result
    }

}
