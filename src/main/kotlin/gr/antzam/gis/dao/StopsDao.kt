package gr.antzam.gis.dao

import gr.antzam.gis.exceptions.MmsiNotFoundException
import gr.antzam.gis.exceptions.StopNotFoundException
import gr.antzam.gis.models.TimeBoundary
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import java.util.*
import javax.sql.DataSource

@Repository
open class StopsDao @Autowired constructor(dataSource: DataSource) {

    private val jdbcTemplate = JdbcTemplate(dataSource)

    open fun getAllShipsMmsis(): List<Int> {
        val queryString = "SELECT mmsi FROM stops GROUP BY mmsi ORDER BY mmsi"
        val result = jdbcTemplate.queryForList(queryString, Int::class.java)
        return result
    }

    open fun getStopBoundariesForMmsi(mmsi: Int): List<TimeBoundary> {
        val queryString = """
            SELECT stop, min(t) AS start, max(t) AS "end"
            FROM stops
            WHERE mmsi = ?
            GROUP BY stop
            ORDER BY stop
        """
        val result = jdbcTemplate.query(queryString,
                RowMapper<TimeBoundary> { rs, rowNum ->
                    TimeBoundary(
                            id = rs?.getInt("stop") ?: rowNum,
                            start = Date(rs?.getTimestamp("start")?.time ?: 0),
                            end = Date(rs?.getTimestamp("end")?.time ?: 0)
                    )
                },
                mmsi)
        if (result.size == 0) {
            throw MmsiNotFoundException("No vessel with MMSI $mmsi found")
        }
        return result
    }

    open fun getStopConvexHullAsGeoJson(mmsi: Int, stop: Int): String {
        val queryString = """
            SELECT ST_AsGeoJSON(ST_Transform(ST_ConvexHull(ST_Union(position)), 4326))
            FROM positions AS p JOIN stops AS s ON p.mmsi = s.mmsi AND p.t = s.t
            WHERE s.mmsi = ? AND s.stop = ?
            GROUP BY s.stop
        """
        try {
            val result = jdbcTemplate.queryForObject(queryString, String::class.java, mmsi, stop)
            return result
        } catch (_: EmptyResultDataAccessException) {
            throw StopNotFoundException("Stop $stop not found for vessel with MMSI $mmsi")
        }
    }

}
