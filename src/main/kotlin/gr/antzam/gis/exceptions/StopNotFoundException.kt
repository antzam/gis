package gr.antzam.gis.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such stop")
class StopNotFoundException : IllegalArgumentException {
    constructor() : super()
    constructor(s: String) : super(s)
    constructor(message: String, cause: Throwable) : super(message, cause)
    constructor(cause: Throwable) : super(cause)
}
