package gr.antzam.gis.controllers

import gr.antzam.gis.dao.StopsDao
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/stops")
open class StopsController @Autowired constructor(private val stopsDao: StopsDao) {

    @GetMapping("/mmsis.json")
    fun getMmsis() = stopsDao.getAllShipsMmsis()

    @GetMapping("/{mmsi}/stops.json")
    fun getStopsForMmsi(@PathVariable mmsi: Int) = stopsDao.getStopBoundariesForMmsi(mmsi)

    @GetMapping("/{mmsi}/{stop}/stop.json")
    fun getStopGeoJson(@PathVariable mmsi: Int, @PathVariable stop: Int)
            = stopsDao.getStopConvexHullAsGeoJson(mmsi, stop)

}
