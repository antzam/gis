package gr.antzam.gis.controllers

import gr.antzam.gis.dao.TrajectoriesDao
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/trajectories")
open class TrajectoriesController @Autowired constructor(private val trajectoriesDao: TrajectoriesDao) {

    @GetMapping("/mmsis.json")
    fun getMmsis() = trajectoriesDao.getAllShipsMmsi()

    @GetMapping("/{mmsi}/trajectories.json")
    fun getTrajectoriesForMmsi(@PathVariable mmsi: Int) = trajectoriesDao.getTrajectoryBoundariesForMmsi(mmsi)

    @GetMapping("/{mmsi}/{trajectory}/trajectory.json")
    fun getTrajectoryAsGeoJson(@PathVariable mmsi: Int, @PathVariable trajectory: Int) =
            trajectoriesDao.getTrajectoryAsGeoJson(mmsi, trajectory)

    @GetMapping("/{mmsi}/{trajectory}/positions.json")
    fun getPositionsWithSpeedsForTrajectory(@PathVariable mmsi: Int, @PathVariable trajectory: Int) =
            trajectoriesDao.getPositionsWithSpeedsForTrajectory(mmsi, trajectory)

}
