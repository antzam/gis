package gr.antzam.gis.controllers

import gr.antzam.gis.dao.PositionsDao
import gr.antzam.gis.models.Position
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.ZonedDateTime

@RestController
@RequestMapping("/positions")
open class PositionsController @Autowired constructor(private val positionsDao: PositionsDao) {

    @GetMapping("/{dateTime}")
    fun getPositionsAtInstant(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) dateTime: ZonedDateTime): List<Position> {
        val instant = dateTime.toInstant()
        return positionsDao.getPositionsAtInstant(instant)
    }

}
