package gr.antzam.gis.models

import java.util.*

data class TimeBoundary(
        val id: Int,
        val start: Date,
        val end: Date
)
