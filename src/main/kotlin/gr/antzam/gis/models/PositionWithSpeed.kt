package gr.antzam.gis.models

import java.util.*

data class PositionWithSpeed(
        var time: Date,
        val lon: Float,
        val lat: Float,
        val speed: Float
)
