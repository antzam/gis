package gr.antzam.gis.models

data class Position(
        val mmsi: Int,
        val lon: Float,
        val lat: Float
)
