CREATE TABLE raw_data (
  timestamp DOUBLE PRECISION NOT NULL,
  type      INTEGER          NOT NULL,
  mmsi      INTEGER          NOT NULL,
  status    INTEGER,
  lon       DOUBLE PRECISION NOT NULL,
  lat       DOUBLE PRECISION NOT NULL,
  heading   INTEGER,
  turn      DOUBLE PRECISION,
  speed     DOUBLE PRECISION,
  course    DOUBLE PRECISION
);
