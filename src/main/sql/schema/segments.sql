CREATE TABLE segments (
  t       TIMESTAMP WITH TIME ZONE NOT NULL,
  mmsi    INTEGER                  NOT NULL,
  segment INTEGER                  NOT NULL,

  PRIMARY KEY (mmsi, segment, t),
  FOREIGN KEY (mmsi, t) REFERENCES positions (mmsi, t) ON DELETE CASCADE
);

CREATE INDEX ON segments (mmsi, t);
