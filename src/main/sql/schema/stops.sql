CREATE TABLE stops (
  t    TIMESTAMP WITH TIME ZONE NOT NULL,
  mmsi INTEGER                  NOT NULL,
  stop INTEGER                  NOT NULL,

  PRIMARY KEY (mmsi, t, stop),
  FOREIGN KEY (mmsi, t) REFERENCES positions ON DELETE CASCADE
);

CREATE INDEX ON stops (mmsi, t);
