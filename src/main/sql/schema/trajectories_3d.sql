CREATE MATERIALIZED VIEW trajectories_3d AS
  SELECT
    mmsi, trajectory, ST_MakeLine(point_3d ORDER BY t) AS geometry
  FROM (SELECT
          mmsi, trajectory, t,
          ST_SetSRID(ST_MakePointM(ST_X(position), ST_Y(position), extract(EPOCH FROM t)), 2100) AS point_3d
        FROM positions NATURAL JOIN trajectories
        ORDER BY mmsi, trajectory, t
       ) AS p
  GROUP BY mmsi, trajectory;

CREATE INDEX ON trajectories_3d USING GIST (geometry);
CREATE INDEX ON trajectories_3d USING GIST (geometry gist_geometry_ops_nd);
