CREATE TABLE trajectories (
  t          TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  mmsi       INTEGER                     NOT NULL,
  trajectory INTEGER                     NOT NULL,

  PRIMARY KEY (mmsi, trajectory, t),
  FOREIGN KEY (mmsi, t) REFERENCES positions (mmsi, t) ON DELETE CASCADE
);

CREATE INDEX ON trajectories (mmsi, t);
