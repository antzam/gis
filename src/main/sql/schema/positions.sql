CREATE TABLE positions (
  t        TIMESTAMP WITH TIME ZONE NOT NULL,
  type     INTEGER                  NOT NULL,
  mmsi     INTEGER                  NOT NULL,
  status   INTEGER,
  position GEOMETRY(POINT, 2100)    NOT NULL,
  heading  INTEGER,
  turn     DOUBLE PRECISION,
  speed    DOUBLE PRECISION,
  course   DOUBLE PRECISION,

  CONSTRAINT positions_pkey PRIMARY KEY (mmsi, t)
);

CREATE INDEX ON positions (t);
CREATE INDEX ON positions USING GIST (position);
