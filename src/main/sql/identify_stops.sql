WITH stop_positions AS (
  SELECT t, mmsi
  FROM (SELECT
          mmsi, t,
          CASE
            WHEN lag(t, 1) OVER w IS NULL THEN
              ST_Distance(lead(position, 1) OVER w, position) / extract(EPOCH FROM lead(t, 1) OVER w - t)
            WHEN lead(t, 1) OVER w IS NULL THEN
              ST_Distance(position, lag(position, 1) OVER w) / extract(EPOCH FROM t - lag(t, 1) OVER w)
            ELSE
              ST_Distance(lead(position, 1) OVER w, lag(position, 1) OVER w)
                / extract(EPOCH FROM lead(t, 1) OVER w - lag(t, 1) OVER w)
          END AS speed
        FROM positions NATURAL JOIN segments
        WINDOW w AS (PARTITION BY mmsi, segment ORDER BY t)) AS speeds
  WHERE speed < 1
)
INSERT INTO stops (t, mmsi, stop)
  SELECT
    t, mmsi, sum(boundary) OVER (PARTITION BY mmsi ORDER BY t) AS stop
  FROM (SELECT
          p.mmsi, p.t,
          CASE
            WHEN lag(p.t, 1) OVER (PARTITION BY p.mmsi, p.segment ORDER BY p.t) IS NULL
              THEN 1
            WHEN p.t - lag(p.t, 1) OVER (PARTITION BY p.mmsi, p.segment ORDER BY p.t) > '15 mins'
              THEN 1
            ELSE 0
          END AS boundary
        FROM (positions NATURAL JOIN segments) AS p JOIN stop_positions AS s
          ON p.mmsi = s.mmsi AND p.t = s.t) AS stop_boundaries;
