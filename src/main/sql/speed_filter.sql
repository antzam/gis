DELETE FROM positions
WHERE (mmsi, t) = ANY (
  SELECT mmsi, t
  FROM (SELECT
          mmsi, t,
          ST_Distance(position, lag(position, 1) OVER w) / extract(EPOCH FROM t - lag(t, 1) OVER w) AS speed
        FROM positions NATURAL JOIN segments
        WINDOW w AS (PARTITION BY mmsi, segment ORDER BY t)) AS p
  WHERE speed > 40);
