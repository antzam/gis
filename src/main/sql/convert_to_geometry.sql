INSERT INTO positions (t, type, mmsi, status, position, heading, turn, speed, course)
  SELECT
    to_timestamp(timestamp),
    type,
    mmsi,
    status,
    ST_Transform(ST_SetSRID(ST_MakePoint(lon, lat), 4326), 2100),
    heading,
    turn,
    speed,
    course
  FROM raw_data
  WHERE lon BETWEEN 18.27 AND 29.97 AND lat BETWEEN 33.23 AND 41.77
ON CONFLICT ON CONSTRAINT positions_pkey DO NOTHING;
