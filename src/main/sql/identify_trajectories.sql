WITH stop_boundaries AS (
    SELECT p.mmsi, s.stop, min(p.t) AS start, max(p.t) AS "end"
    FROM positions AS p JOIN stops AS s ON p.mmsi = s.mmsi AND p.t = s.t
    GROUP BY p.mmsi, s.stop
)
INSERT INTO trajectories (t, mmsi, trajectory)
  SELECT
    t, mmsi, sum(boundary) OVER (PARTITION BY mmsi ORDER BY t) AS trajectory
  FROM (SELECT
          p.t, p.mmsi, sb.start,
          CASE
            WHEN lag(p.t, 1) OVER (PARTITION BY p.mmsi, s.segment ORDER BY p.t) IS NULL
              THEN 1
            WHEN lag(sb.end, 1) OVER (PARTITION BY p.mmsi, s.segment ORDER BY p.t) IS NOT NULL
              THEN 1
            ELSE 0
          END AS boundary
        FROM positions AS p
          JOIN segments AS s ON p.mmsi = s.mmsi AND p.t = s.t
          LEFT OUTER JOIN stop_boundaries AS sb
            ON p.mmsi = sb.mmsi AND p.t BETWEEN sb.start AND sb.end) AS ss
  WHERE ss.start IS NULL;
