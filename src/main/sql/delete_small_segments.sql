DELETE FROM segments
WHERE (mmsi, segment) = ANY (SELECT mmsi, segment
                             FROM segments
                             GROUP BY mmsi, segment
                             HAVING count(t) < 10);
