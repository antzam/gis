INSERT INTO segments (t, mmsi, segment)
  SELECT
    t, mmsi,
    sum(boundary) OVER (PARTITION BY mmsi ORDER BY t) AS segment
  FROM (SELECT
          t, mmsi,
          CASE
            WHEN lag(t, 1) OVER (PARTITION BY mmsi ORDER BY t) IS NULL
              THEN 1
            WHEN t - lag(t, 1) OVER (PARTITION BY mmsi ORDER BY t) > '30 mins'
              THEN 1
            ELSE 0
          END AS boundary
        FROM positions) AS b;
