angular.module('gisApp', [
  'ngRoute',
  'trajectories',
  'stops',
  'positions'
])
  .config(['$locationProvider', '$routeProvider',
    function ($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider
        .when('/trajectories', {
          template: '<trajectories></trajectories>'
        })
        .when('/stops', {
          template: '<stops></stops>'
        })
        .when('/positions', {
          template: '<positions></positions>'
        })
        .otherwise('/positions');
    }
  ]);
