angular.module('positions', ['map'])
  .component('positions', {
    templateUrl: 'js/positions/positions.template.html',
    controller: ['$scope', '$http', function ($scope, $http) {
      $scope.selectedDate = moment('2016-11-01T02:00:00+02:00');
      $scope.positions = [];

      angular.element('#datetimepicker').datetimepicker({
        locale: "el",
        minDate: "2016-11-01",
        maxDate: "2016-12-31",
        inline: true,
        sideBySide: false,
        useCurrent: false,
        defaultDate: moment('2016-11-01T02:00:00+02:00'),
        format: "YYYY-MM-DD HH:mm"
      }).on('dp.change', function (e) {
        $scope.selectedDate = e.date;
        $scope.dateChanged();
      });

      $scope.dateChanged = function () {
        $http.get('/positions/' + $scope.selectedDate.format())
          .then(function (response) {
            $scope.positions = response.data;
          }, function () {
            $scope.positions = [];
          });
      };
    }]
  });
