angular.module('stops', ['map'])
  .component('stops', {
    templateUrl: 'js/stops/stops.template.html',
    controller: ['$scope', '$http', function ($scope, $http) {
      $http.get('/stops/mmsis.json').then(function (response) {
        $scope.mmsis = response.data;
      });

      $scope.selectedMmsi = 0;
      $scope.selectedStop = 0;
      $scope.stops = [];
      $scope.stop = {};

      $scope.mmsiChanged = function () {
        $http.get('/stops/' + $scope.selectedMmsi + '/stops.json')
          .then(function (response) {
            $scope.stops = response.data;
          }, function () {
            $scope.stops = [];
          })
          .finally(function () {
            $scope.selectedStop = 0;
            $scope.stop = {};
          });
      };

      $scope.stopChanged = function () {
        $http.get('/stops/' + $scope.selectedMmsi + '/' + $scope.selectedStop + '/stop.json')
          .then(function (response) {
            $scope.stop = response.data;
          }, function () {
            $scope.stop = {};
          });
      }
    }]
  });
