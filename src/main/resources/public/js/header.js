angular.module('gisApp')
  .controller('HeaderController', ['$scope', '$location', '$route', function ($scope,$location, $route) {
    $scope.$on('$routeChangeSuccess', function () {
      $scope.activeTab = $location.path().replace(/^\//, "");
    });
  }]);
