angular.module('trajectories', ['map'])
  .component('trajectories', {
    templateUrl: 'js/trajectories/trajectories.template.html',
    controller: ['$scope', '$http', function ($scope, $http) {
      $http.get('/trajectories/mmsis.json').then(function (response) {
        $scope.mmsis = response.data;
      });

      $scope.selectedMmsi = 0;
      $scope.selectedTrajectory = 0;
      $scope.trajectories = [];
      $scope.trajectory = {};

      $scope.mmsiChanged = function () {
        $http.get('/trajectories/' + $scope.selectedMmsi + '/trajectories.json')
          .then(function (response) {
            $scope.trajectories = response.data;
          }, function () {
            $scope.trajectories = [];
          })
          .finally(function () {
            $scope.selectedTrajectory = 0;
            $scope.trajectory = {};
          });
      };

      $scope.trajectoryChanged = function () {
        var trajectory = {};
        $http.get('/trajectories/' + $scope.selectedMmsi + '/' + $scope.selectedTrajectory + '/trajectory.json')
          .then(function (response) {
            trajectory.geoJson = response.data;
            return $http.get('/trajectories/' + $scope.selectedMmsi + '/' + $scope.selectedTrajectory +
              '/positions.json')
          }, function () {
            trajectory = {};
          })
          .then(function (response) {
            trajectory.positions = response.data;
          }, function () {
            trajectory = {};
          })
          .finally(function () {
            $scope.trajectory = trajectory;
          });
      };
    }]
  });
