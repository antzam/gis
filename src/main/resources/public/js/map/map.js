angular.module('map', [])
  .directive('map', ['$filter', function ($filter) {
    function link(scope, element, attrs) {
      var map = L.map("map").setView([37.7, 23.5], 10);
      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
        zoom: 10,
        minZoom: 10,
        maxZoom: 15
      }).addTo(map);

      var positions = [];
      var trajectory = {};
      var stop = {};

      var layers = [];

      scope.$watch(attrs.positions, function (value) {
        positions = value;
        redrawMap();
      });

      scope.$watch(attrs.trajectory, function (value) {
        trajectory = value;
        redrawMap();
      });

      scope.$watch(attrs.stop, function (value) {
        stop = value;
        redrawMap();
      });

      function redrawMap() {
        angular.forEach(layers, function (layer) {
          map.removeLayer(layer);
        });
        layers = [];
        angular.forEach(positions, function (position) {
          var marker = L.marker([position.lat, position.lon]);
          marker.bindPopup('MMSI: ' + position.mmsi);
          marker.addTo(map);
          layers.push(marker);
        });
        if (!angular.isUndefined(trajectory) && !angular.equals(trajectory, {})) {
          var trajectoryLayer = L.geoJSON(trajectory.geoJson);
          trajectoryLayer.addTo(map);
          layers.push(trajectoryLayer);

          angular.forEach(trajectory.positions, function (position) {
            var circle = L.circle([position.lat, position.lon]).addTo(map);
            circle.bindPopup('Ώρα: ' + $filter('date')(position.time, 'yyyy-MM-dd HH:mm:ss') +
              '<br>Ταχύτητα: ' + position.speed.toPrecision(3) + ' kn');
            layers.push(circle);
          });
        }
        if (!angular.isUndefined(stop) && !angular.equals(stop, {})) {
          var stopLayer = L.geoJSON(stop);
          stopLayer.addTo(map);
          layers.push(stopLayer);
        }
      }
    }

    return {
      restrict: 'E',
      scope: {
        positions: '=',
        trajectory: '=',
        stop: '='
      },
      template: '<div id="map"></div>',
      link: link
    }
  }]);
